var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PhoneSchema  = new Schema({
    tel:        { type: String, required: true, unique: true },
    regular:    { type: Number, default: 0, min: 0},
    fake:       { type: Number, default: 0, min: 0},
    created_at: Date,
    updated_at: Date
});

var Phone = mongoose.model('User', PhoneSchema);

module.exports = mongoose.model('Phone', Phone);
