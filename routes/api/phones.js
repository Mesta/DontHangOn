var express = require('express');
var router  = express.Router();


/* GET phone listing. */
router.get('/', function(req, res, next) {
    res.contentType('application/json');
    res.json('toto');
});

/* POST new phone number */
router.post('/', function(req, res, next) {
    res.contentType('application/json');
    // res.json('toto');
    var tel = req.body.tel;
    res.json(tel + "toto");
});

module.exports = router;
